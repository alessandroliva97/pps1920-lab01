package lab01.tdd;

public class SimpleSelectStrategyFactory implements SelectStrategyFactory {

    @Override
    public SelectStrategy evenStrategy() {
        return multipleOfStrategy(2);
    }

    @Override
    public SelectStrategy multipleOfStrategy(final int value) {
        return e -> e % value == 0;
    }

    @Override
    public SelectStrategy equalsStrategy(final int value) {
        return e -> e == value;
    }

}
