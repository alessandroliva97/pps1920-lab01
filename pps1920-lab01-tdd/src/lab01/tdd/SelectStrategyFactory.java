package lab01.tdd;

public interface SelectStrategyFactory {

    /**
     * Creates a strategy to find even elements
     * @return a strategy to find even elements
     */
    public SelectStrategy evenStrategy();

    /**
     * Creates a strategy to find elements multiple of a value
     * @param value the value to be multiple of
     * @return a strategy to find elements multiple of a value
     */
    public SelectStrategy multipleOfStrategy(final int value);

    /**
     * Creates a strategy to find elements equal to a value
     * @param value the value to be equal to
     * @return a strategy to find elements equal to a value
     */
    public SelectStrategy equalsStrategy(final int value);

}
