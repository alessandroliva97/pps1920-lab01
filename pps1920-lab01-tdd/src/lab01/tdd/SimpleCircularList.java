package lab01.tdd;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class SimpleCircularList implements CircularList {

    private List<Integer> list;
    private int pointer;

    /*
     *  After a reset, next() has to point to the first element, while previous() to the last element,
     *  to do so, an "in between" pointer is temporary needed
     */
    private boolean neutralIndex;

    public SimpleCircularList() {
        this.list = new ArrayList<>();
        this.pointer = 0;
    }

    @Override
    public void add(int element) {
        this.pointer++;
        list.add(element);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public Optional<Integer> next() {
        if (neutralIndex) {
            pointer = list.size() - 1;
            neutralIndex = false;
        }
        pointer++;
        pointer = pointer > list.size() - 1 ? 0 : pointer;
        return get();
    }

    @Override
    public Optional<Integer> previous() {
        if (neutralIndex) {
            pointer = 0;
            neutralIndex = false;
        }
        pointer--;
        pointer = pointer < 0 ? list.size() - 1 : pointer;
        return get();
    }

    private Optional<Integer> get() {
        if (list.isEmpty())
            return Optional.empty();
        else
            return Optional.of(list.get(pointer));
    }

    @Override
    public void reset() {
        this.neutralIndex = true;
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        return Stream.generate(this::next)
                .limit(size())
                .map(Optional::get)
                .filter(strategy::apply)
                .findFirst();
    }

}
