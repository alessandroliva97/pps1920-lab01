import com.sun.org.apache.bcel.internal.generic.Select;
import lab01.tdd.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {

    private static final int ADDITIONS = 5;
    private static final int LAPS = 3;

    private CircularList circularList;

    @BeforeEach
    void beforeEach() {
        circularList = new SimpleCircularList();
    }

    @Test
    public void testListIsEmpty() {
        assertTrue(circularList.isEmpty());
    }

    @Test
    public void testEmptyListNextElement() {
        assertEquals(Optional.empty(), circularList.next());
    }

    @Test
    public void testEmptyListPreviousElement() {
        assertEquals(Optional.empty(), circularList.previous());
    }

    @Test
    public void testAddElement() {
        circularList.add(0);
        assertEquals(Optional.of(0), circularList.next());
    }

    @Test
    public void testListSize() {
        IntStream.range(0, ADDITIONS).forEach(i -> circularList.add(i));
        assertEquals(ADDITIONS, circularList.size());
    }

    @Test
    public void testNextWhileEmpty() {
        assertEquals(Optional.empty(), circularList.next());
    }

    @Test
    public void testPreviousWhileEmpty() {
        assertEquals(Optional.empty(), circularList.previous());
    }

    @Test
    public void testNextElement() {
        circularList.add(0);
        assertEquals(Optional.of(0), circularList.next());
    }

    @Test
    public void testPreviousElement() {
        circularList.add(0);
        assertEquals(Optional.of(0), circularList.previous());
    }

    @Test
    public void testResetThenGetNext() {
        addSomeElementsThenReset();
        assertEquals(Optional.of(0), circularList.next());
    }

    @Test
    public void testResetThenGetPrevious() {
        addSomeElementsThenReset();
        assertEquals(Optional.of(4), circularList.previous());
    }

    private void addSomeElementsThenReset() {
        IntStream.range(0, ADDITIONS).forEach(i -> circularList.add(i));
        circularList.reset();
    }

    @Test
    public void testForwardCircularity() {
        IntStream.range(0, ADDITIONS).forEach(i -> circularList.add(i));
        IntStream.range(0, ADDITIONS * LAPS).forEach(i -> circularList.next());
        assertEquals(Optional.of(0), circularList.next());
    }

    @Test
    public void testBackwardCircularity() {
        IntStream.range(0, ADDITIONS).forEach(i -> circularList.add(i));
        IntStream.range(0, ADDITIONS * LAPS).forEach(i -> circularList.previous());
        assertEquals(Optional.of(4), circularList.previous());
    }

    @Test
    public void testEvenStrategy() {
        addSomeElementsThenReset();
        SelectStrategy evenStrategy = (e -> e % 2 == 0);
        circularList.next(evenStrategy);
        assertEquals(Optional.of(2), circularList.next(evenStrategy));
    }

    @Test
    public void testMultipleOfStrategy() {
        addSomeElementsThenReset();
        SelectStrategy multipleOf3Strategy = (e -> e % 3 == 0);
        circularList.next(multipleOf3Strategy);
        assertEquals(Optional.of(3), circularList.next(multipleOf3Strategy));
    }

    @Test
    public void testEqualsStrategy() {
        addSomeElementsThenReset();
        SelectStrategy equalsTo3Strategy = (e -> e == 3);
        assertEquals(Optional.of(3), circularList.next(equalsTo3Strategy));
    }

    @Test
    public void testFactoryEvenStrategy() {
        addSomeElementsThenReset();
        SelectStrategy evenStrategy = new SimpleSelectStrategyFactory().evenStrategy();
        circularList.next(evenStrategy);
        assertEquals(Optional.of(2), circularList.next(evenStrategy));
    }

    @Test
    public void testFactoryMultipleOfStrategy() {
        addSomeElementsThenReset();
        SelectStrategy multipleOf3Strategy = new SimpleSelectStrategyFactory().multipleOfStrategy(4);
        circularList.next(multipleOf3Strategy);
        assertEquals(Optional.of(4), circularList.next(multipleOf3Strategy));
    }

    @Test
    public void testFactoryEqualsStrategy() {
        addSomeElementsThenReset();
        SelectStrategy equalsTo3Strategy = new SimpleSelectStrategyFactory().equalsStrategy(2);
        assertEquals(Optional.of(2), circularList.next(equalsTo3Strategy));
    }

}
