import lab01.example.model.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the SimpleBankAccountWithAtm implementation
 */
class SimpleBankAccountWithAtmTest extends SimpleBankAccountTest {

    private AccountHolder accountHolder;
    private BankAccountWithAtm bankAccount;

    @BeforeEach
    void beforeEach() {
        accountHolder = new AccountHolder("Mario", "Rossi", 1);
        bankAccount = new SimpleBankAccountWithAtm(accountHolder, 0);
        init(accountHolder, bankAccount);
    }

    @Test
    void testDepositFromAtm() {
        bankAccount.depositFromAtm(accountHolder.getId(), 100);
        assertEquals(99, bankAccount.getBalance());
    }

    @Test
    void testWrongDepositFromAtm() {
        bankAccount.depositFromAtm(accountHolder.getId(), 100);
        bankAccount.depositFromAtm(2, 50);
        assertEquals(99, bankAccount.getBalance());
    }

    @Test
    void testWithdrawFromAtm() {
        bankAccount.depositFromAtm(accountHolder.getId(), 100);
        bankAccount.withdrawFromAtm(accountHolder.getId(), 70);
        assertEquals(28, bankAccount.getBalance());
    }

    @Test
    void testWrongWithdrawFromAtm() {
        bankAccount.depositFromAtm(accountHolder.getId(), 100);
        bankAccount.withdrawFromAtm(2, 70);
        assertEquals(99, bankAccount.getBalance());
    }

}
