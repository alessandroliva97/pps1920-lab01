package lab01.example.model;

public class SimpleBankAccountWithAtm extends SimpleBankAccount implements BankAccountWithAtm {

    private final static int ATM_FEE = 1;

    public SimpleBankAccountWithAtm(AccountHolder holder, double balance) {
        super(holder, balance);
    }

    @Override
    public void depositFromAtm(final int usrID, final double amount) {
        super.deposit(usrID, amount);
        payFee(usrID);
    }

    @Override
    public void withdrawFromAtm(final int usrID, final double amount) {
        if (checkIfAtmWithdrawPermitted(amount)) {
            super.withdraw(usrID, amount);
            payFee(usrID);
        }
    }

    protected boolean checkIfAtmWithdrawPermitted(final double amount) { return super.isWithdrawAllowed(amount + ATM_FEE); }

    protected void payFee(final int usrID) { super.withdraw(usrID, ATM_FEE); }

}
